/*
 * =====================================================================================
 *
 *       Filename:  usbHotplug.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2015年04月21日 10時18分57秒
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/version.h>
#include <linux/netdevice.h>

#define UEVENT_BUFFER_SIZE 2048

MODULE_DESCRIPTION("Hello World !!");
MODULE_AUTHOR("Robert Liu");
MODULE_LICENSE("GPL");

static int netdev_hotplug_event(struct notifier_block *this, unsigned long event, void *ptr)
{
    /* LOCAL VARIABLES DECLARATION
     */
    struct net_device *netdev = ptr;

    /* BODY
     */
	printk("Get notice of interface %s\n", ((struct net_device *) ptr)->name);
	 
    if ( event == NETDEV_REGISTER ) {
		if ( memcmp(netdev->name, "eth1", 4) == 0 ) {
			printk("Plug in\n");
		}
    } else if ( (event == NETDEV_UNREGISTER) && (memcmp(netdev->name, "eth1", 4) == 0) ) {
		printk("Plug out\n");
	}
    
    return NOTIFY_OK;
}

static struct notifier_block netdev_hotplug_notifier = {
    .notifier_call = netdev_hotplug_event,
};

static int __init usbHotplug_init(void)
{
	printk(KERN_INFO "usbHotplug Start\n");
	register_netdevice_notifier(&netdev_hotplug_notifier);
	
	return 0;
}

static void __exit usbHotplug_exit(void)
{
	printk(KERN_INFO "usbHotplug Exit\n");
	unregister_netdevice_notifier(&netdev_hotplug_notifier);
}

module_init(usbHotplug_init);
module_exit(usbHotplug_exit);

